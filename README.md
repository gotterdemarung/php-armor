
Installation
============

1. Clone it from `https://bitbucket.org/gotterdemarung/php-armor.git`
2. Cd to it and run `composer install`
3. Symlink from `bin/armor` to your $PATH
4. Register code complete:

```
# BASH ~4.x, ZSH
source <([program] _completion --generate-hook)

# BASH ~3.x, ZSH
[program] _completion --generate-hook | source /dev/stdin

# BASH (any version)
eval $([program] _completion --generate-hook)
```