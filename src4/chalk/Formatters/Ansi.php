<?php

namespace Chalk\Formatters;

use Chalk\AnsiColor as C;
use Chalk\FormatterInterface;
use Chalk\PieceInterface;
use Chalk\Pieces\AnsiColoredString as ColorStr;
use Chalk\Pieces\CompositePiece;
use Chalk\Pieces\EmptyStringPiece;
use Chalk\Pieces\HashMapPiece;
use Chalk\Pieces\DumpPiece;
use Chalk\Pieces\LogEntry;
use Chalk\Pieces\NullPiece;
use Chalk\Pieces\SectionHeader;

class Ansi implements FormatterInterface
{
    /**
     * @var \Chalk\AnsiColor
     */
    private $colors;

    /**
     * Constructor
     *
     * @param bool $colors
     */
    public function __construct($colors)
    {
        $this->colors = $colors ? new C() : null;
    }

    /**
     * Formats string
     *
     * @param PieceInterface $p
     * @return string
     */
    public function format(PieceInterface $p)
    {
        if ($this->colors === null) {
            return $p->__toString();
        }

        // Composites
        if ($p instanceof SectionHeader) {
            return $this->format(new ColorStr($p->getValue(), C::GREEN, null, C::BOLD, true))
                 . $this->format(new ColorStr(str_repeat('=', $p->getLength()), C::GREEN, null, null, true));
        }
        if ($p instanceof HashMapPiece) {
            return $this->fromHashMap($p);
        }
        if ($p instanceof CompositePiece) {
            $buffer = '';
            foreach ($p->getValue() as $innerPiece) {
                $buffer .= $this->format($innerPiece);
            }
            return $buffer;
        }
        if ($p instanceof DumpPiece) {
            return $this->recursiveJson($p->getValue()) . "\n";
        }
        if ($p instanceof LogEntry) {
            $buffer = '';
            $buffer .= $this->format(new ColorStr(date('H:i:s', $p->getTime()), C::MAGENTA, null, null, null));
            $buffer .= ' ';
            switch ($p->getLevel()) {
                case 'debug':
                    $buffer .= $this->format(new ColorStr('d', C::BLACK));
                    break;
                case 'info':
                    $buffer .= $this->format(new ColorStr('i', C::GREEN));
                    break;
                case 'notice':
                    $buffer .= $this->format(new ColorStr('n', C::YELLOW));
                    break;
                case 'warning':
                    $buffer .= $this->format(new ColorStr('W', C::YELLOW));
                    break;
                case 'error':
                    $buffer .= $this->format(new ColorStr('E', C::RED));
                    break;
                default:
                    $letter = strtoupper(substr($p->getLevel(), 0, 1));
                    $buffer .= $this->format(new ColorStr($letter, C::WHITE, C::RED, C::BOLD));
            }
            $buffer .= ' ' . $this->interpolate($p->getMessage(), $p->getContext());
            $buffer .= "\n";
            return $buffer;
        }

        // Primitives
        switch (true) {
            case $p instanceof ColorStr:
                return $this->colors->color($p->getForeground(), $p->getBackground(), $p->getSpecial())
                     . $p->getValue()
                     . $this->colors->reset()
                     . ($p->isNewline() ? "\n" : '');
            case $p instanceof EmptyStringPiece:
                return $this->format(new ColorStr('<empty string>', C::WHITE, C::BLACK, C::ITALIC));
            case $p instanceof NullPiece:
                return $this->format(new ColorStr('<null>', C::WHITE, C::BLACK, C::ITALIC));
            default:
                return $p->__toString();
        }
    }

    public function interpolate($message, array $context)
    {
        if (!is_string($message) || !is_array($context) || count($context) === 0) {
            // Something wrong - fallback mode
            return $message;
        }

        return preg_replace_callback(
            '/[ ,\.]:([0-9a-z\.\-_]+)/i',
            function ($matches) use ($context) {
                $key = $matches[1];
                if (!isset($context[$key]) || !is_scalar($context[$key])) {
                    return $this->format(new ColorStr($matches[0], C::WHITE, null, C::BOLD));
                }

                $value = $context[$key];
                if ($value === null) {
                    return $this->format(new NullPiece());
                } elseif (is_bool($value)) {
                    return ' ' . $this->format(new ColorStr($value, C::CYAN, null, C::BOLD));
                } else {
                    return ' ' . $this->format(new ColorStr($value, C::GREEN, null, C::BOLD));
                }
            },
            $message
        );
    }

    public function fromHashMap(HashMapPiece $piece)
    {
        $buffer = '';

        $widthKey = null;
        $widthType = null;

        foreach ($piece->getValue() as $k => $v) {
            if ($widthKey === null || strlen($k) > $widthKey) {
                $widthKey = strlen($k);
            }
            if ($piece->isShowTypesEnabled()) {
                if ($widthType === null || strlen(gettype($v)) > $widthType) {
                    $widthType = strlen(gettype($v));
                }
            }
        }

        // Printing
        foreach ($piece->getValue() as $k => $v) {
            $key = $k . str_repeat(' ', $widthKey - strlen($k));
            $buffer .= $this->format(new ColorStr($key, C::YELLOW));
            $buffer .= ' | ';
            if ($widthType !== null) {
                $type = gettype($v);
                $type = $type . str_repeat(' ', $widthType - strlen($type));
                $buffer .= $this->format(new ColorStr($type, C::BLUE));
                $buffer .= ' | ';
            }
            if ($v === null) {
                $buffer .= $this->format(new NullPiece());
            } elseif (is_string($v) && empty($v)) {
                $buffer .= $this->format(new EmptyStringPiece());
            } elseif (is_array($v)) {
                $buffer .= print_r($v, true);
            } else {
                $buffer .= (string) $v;
            }
            $buffer .= "\n";
        }

        return $buffer;
    }

    private function recursiveJson($data, $depth = 0)
    {
        $tab = str_repeat('  ', $depth);

        if ($data === null) {
            return $this->format(new NullPiece());
        }
        if (is_string($data) && empty($data)) {
            return $this->format(new EmptyStringPiece());
        }
        if (is_bool($data)) {
            return $this->format(new ColorStr($data ? 'true' : 'false', C::BLUE, null, C::BOLD));
        }
        if (is_scalar($data)) {
            return $this->format(
                new ColorStr('(' . $this->shortScalarType(gettype($data)) . ')', C::BLUE)
            ) . ' ' . trim($data);
        }
        if (is_array($data) && count($data) === 0) {
            return $this->format(new ColorStr('[]', C::BLUE, C::BLACK, C::BOLD));
        }
        if (is_array($data)) {
            $buffer = '';

            // Check if list
            $list = true;
            $i = 0;
            foreach ($data as $k => $v) {
                if (!is_int($k) || $k !== $i++) {
                    $list = false;
                    break;
                }
            }

            foreach ($data as $k => $v) {
                $buffer .= $tab;
                if ($list) {
                    $buffer .= $this->format(new ColorStr(' - ', C::CYAN));
                } else {
                    $buffer .= $this->format(new ColorStr($k, C::YELLOW)) . ': ';
                }
                $buffer .=  (is_array($v) ? "\n" : '')
                    . $this->recursiveJson($v, $depth + 1)
                    . (is_array($v) ? '' : "\n");
            }
            return $buffer;
        }

        return gettype($data);
    }

    public function shortScalarType($type)
    {
        switch ($type) {
            case 'integer':
                return 'int';
            case 'string':
                return 'str';
            case 'boolean':
                return 'bool';
            default:
                return $type;
        }
    }
}
