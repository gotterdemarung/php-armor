<?php

namespace Chalk;

interface FormatterInterface
{
    /**
     * Formats string
     *
     * @param PieceInterface $piece
     * @return string
     */
    public function format(PieceInterface $piece);
}
