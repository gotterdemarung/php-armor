<?php

namespace Chalk;

use Chalk\Formatters\Ansi;

class StreamWriter
{
    /**
     * @var resource
     */
    private $stream;

    /**
     * @var FormatterInterface
     */
    private $formatter;

    /**
     * @var int
     */
    private $level;

    /**
     * @param $output
     * @param FormatterInterface $formatter
     * @return StreamWriter
     */
    public static function buildFromSymfonyOutput($output, FormatterInterface $formatter = null)
    {
        if (!is_object($output)) {
            throw new \InvalidArgumentException('$output must be object');
        }
        if ($output instanceof \Symfony\Component\Console\Output\StreamOutput) {
            if ($formatter === null) {
                $formatter = new Ansi($output->isDecorated());
            }
            $level = 1;
            $stream = $output->getStream();
            switch ($output->getVerbosity()) {
                case 4: // DEBUG
                    $level = -2;
                    break;
                case 3: // VERY VERBOSE
                    $level = -1;
                    break;
                case 2: // VERBOSE
                    $level = 0;
                    break;
                case 0: // QUIET
                    $stream = null;
                    break;
            }
            return new StreamWriter($stream, $formatter, $level);
        }

        throw new \InvalidArgumentException('Not implemented');
    }

    /**
     * @param resource $stream
     * @param FormatterInterface $formatter
     * @param int $level
     */
    public function __construct($stream, FormatterInterface $formatter, $level = 1)
    {
        if (!is_resource($stream) && $stream !== null) {
            throw new \InvalidArgumentException('$stream must be valid resource');
        }
        if (!is_int($level)) {
            throw new \InvalidArgumentException('$level must be integer');
        }

        $this->level = $level;
        $this->stream = $stream;
        $this->formatter = $formatter;
    }

    public function withDeltaThreshold($delta)
    {
        return $this->withThreshold($this->level + $delta);
    }

    public function withThreshold($newLevel)
    {
        return new StreamWriter($this->stream, $this->formatter, $newLevel);
    }

    public function allowed($level)
    {
        return $this->stream !== null && is_int($level) && $level >= $this->level;
    }

    public function writeln($message = '', $level = 1)
    {
        if ($this->allowed($level)) {
            fputs($this->stream, $message . "\n");
        }
        return $this;
    }

    public function writep(PieceInterface $piece, $level = 1)
    {
        if ($this->allowed($level)) {
            fputs($this->stream, $this->formatter->format($piece));
        }
        return $this;
    }
}
