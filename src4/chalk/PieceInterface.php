<?php

namespace Chalk;

interface PieceInterface
{
    /**
     * @return mixed
     */
    public function getValue();

    /**
     * Must be implicitly implemented
     *
     * @return string
     */
    public function __toString();
}
