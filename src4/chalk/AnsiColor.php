<?php

namespace Chalk;

class AnsiColor
{
    const OFF = 0;
    const BOLD = 1;
    const ITALIC = 3;
    const UNDERLINE = 4;
    const BLINK = 5;
    const INVERSE = 7;
    const HIDDEN = 8;

    const BLACK = 30;
    const RED = 31;
    const GREEN = 32;
    const YELLOW = 33;
    const BLUE = 34;
    const MAGENTA = 35;
    const CYAN = 36;
    const WHITE = 37;

    public function reset()
    {
        return "\033[0m";
    }

    public function color($fg = null, $bg = null, $flag = null)
    {
        $result = '';
        if ($fg !== null) {
            if ($flag !== null) {
                $result .= "\033[{$flag};{$fg}m";
            } else {
                $result .= "\033[{$fg}m";
            }
        }
        if ($bg !== null) {
            $result .= "\033[" . ($bg + 10) .'m';
        }

        return $result;
    }
}
