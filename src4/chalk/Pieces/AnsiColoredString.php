<?php

namespace Chalk\Pieces;

use Chalk\PieceInterface;

class AnsiColoredString implements PieceInterface
{
    private $foreground;
    private $background;
    private $special;

    private $value;

    private $newline;

    public function __construct($value, $foreground = null, $background = null, $special = null, $newline = false)
    {
        $this->foreground = $foreground;
        $this->background = $background;
        $this->special = $special;
        $this->newline = (bool) $newline;
        $this->value = (string) $value;
    }

    /**
     * @return mixed
     */
    public function getForeground()
    {
        return $this->foreground;
    }

    /**
     * @return mixed
     */
    public function getBackground()
    {
        return $this->background;
    }

    /**
     * @return mixed
     */
    public function getSpecial()
    {
        return $this->special;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @return bool
     */
    public function isNewline()
    {
        return $this->newline;
    }

    /**
     * Must be implicitly implemented
     *
     * @return string
     */
    public function __toString()
    {
        return $this->value;
    }
}
