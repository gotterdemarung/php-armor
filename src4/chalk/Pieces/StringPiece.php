<?php

namespace Chalk\Pieces;

use Chalk\PieceInterface;

class StringPiece implements PieceInterface
{
    private $value;

    public function __construct($value)
    {
        $this->value = (string) $value;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Must be implicitly implemented
     *
     * @return string
     */
    public function __toString()
    {
        return $this->value;
    }
}
