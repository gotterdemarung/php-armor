<?php

namespace Chalk\Pieces;

use Chalk\PieceInterface;

class LogEntry implements PieceInterface
{
    private $time;
    private $level;
    private $message;
    private $context;

    public function __construct($level, $message, array $context = [], $time = null)
    {
        $this->time = empty($time) ? microtime(true) : floatval($time);
        $this->level = $level;
        $this->message = strval($message);
        $this->context = $context;
    }

    /**
     * @return float|mixed
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * @return mixed
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @return array
     */
    public function getContext()
    {
        return $this->context;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->getMessage();
    }

    /**
     * Must be implicitly implemented
     *
     * @return string
     */
    public function __toString()
    {
        return $this->getMessage();
    }
}
