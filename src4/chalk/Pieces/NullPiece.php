<?php

namespace Chalk\Pieces;

class NullPiece extends AbstractPiece
{
    /**
     * @return mixed
     */
    public function getValue()
    {
        return 'null';
    }
}
