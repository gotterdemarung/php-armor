<?php

namespace Chalk\Pieces;

use Chalk\PieceInterface;

class HashMapPiece implements PieceInterface
{
    private $map;
    private $showTypes;

    public function __construct(array $map, $showValues = false)
    {
        $this->map = $map;
        $this->showTypes = boolval($showValues);
    }

    /**
     * @return mixed
     */
    public function isShowTypesEnabled()
    {
        return $this->showTypes;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->map;
    }

    /**
     * Must be implicitly implemented
     *
     * @return string
     */
    public function __toString()
    {
        return print_r($this->map, true);
    }
}
