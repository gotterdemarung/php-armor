<?php

namespace Chalk\Pieces;

use Chalk\PieceInterface;

class SectionHeader implements PieceInterface
{
    private $value;

    public function __construct($value)
    {
        $this->value = $value;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    public function getLength()
    {
        return strlen($this->__toString());
    }

    public function __toString()
    {
        return (string) $this->value;
    }
}
