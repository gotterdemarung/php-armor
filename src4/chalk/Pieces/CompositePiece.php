<?php

namespace Chalk\Pieces;

use Chalk\PieceInterface;

class CompositePiece implements PieceInterface
{
    /**
     * @var PieceInterface[]
     */
    private $pieces = [];

    public function __construct(array $pieces)
    {
        $this->pieces = $pieces;
    }

    /**
     * @return PieceInterface[]
     */
    public function getValue()
    {
        return $this->pieces;
    }

    /**
     * Must be implicitly implemented
     *
     * @return string
     */
    public function __toString()
    {
        $strings = [];
        foreach ($this->pieces as $p) {
            $strings[] = $p->__toString();
        }

        return implode(' ', $strings);
    }
}
