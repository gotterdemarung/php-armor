<?php

namespace Chalk\Pieces;

use Chalk\PieceInterface;

abstract class AbstractPiece implements PieceInterface
{
    /**
     * Must be implicitly implemented
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getValue();
    }
}
