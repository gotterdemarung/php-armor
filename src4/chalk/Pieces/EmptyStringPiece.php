<?php

namespace Chalk\Pieces;

class EmptyStringPiece extends AbstractPiece
{
    /**
     * @return mixed
     */
    public function getValue()
    {
        return '';
    }
}
