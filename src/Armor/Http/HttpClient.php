<?php

namespace Armor\Http;

use Armor\Http\Chalk\BeforeSendParameter;
use Armor\Http\Chalk\HttpStatusCode;
use Armor\Http\Chalk\ResponseAssertion;
use Armor\Http\Chalk\ResponseHeader;
use Chalk\StreamWriter;
use GuzzleHttp\Client;
use GuzzleHttp\Message\Request;
use GuzzleHttp\Message\Response;
use GuzzleHttp\Message\ResponseInterface;

class HttpClient
{
    /**
     * @var Client
     */
    private $client;
    /**
     * @var StreamWriter|null
     */
    private $writer = null;
    /**
     * @var string|null
     */
    private $proxyServer;

    public function __construct($proxyServer = null, StreamWriter $writer = null)
    {
        if ($proxyServer !== null && !is_string($proxyServer)) {
            throw new \InvalidArgumentException("Proxy server must be string");
        }

        $this->proxyServer = $proxyServer;

        $this->client = new Client();
        $this->writer = $writer;
    }

    /**
     * Sends synchronous HTTP request using Guzzle
     *
     * @param Request $request
     * @return ResponseInterface
     * @throws \Exception
     */
    public function sendGuzzleRequest(Request $request)
    {
        // Perform request
        try {
            $this->showTagged('url', $request->getMethod() . ' ' . $request->getUrl());
            $this->showTagged('dta', (string) $request->getBody(), -1);
            $response = $this->client->send($request);
            if ($this->writer !== null) {
                $this->writer->writep(new HttpStatusCode($response->getStatusCode(), $response->getReasonPhrase()), 2);
            }
        } catch (\Exception $error) {
            if ($this->writer !== null) {
                $this->writer->writep(new HttpStatusCode('---', $error->getMessage()), 2);
            }
            throw $error;
        }

        foreach ($response->getHeaders() as $name => $values) {

            if (strlen($name) < 25) {
                $paddedName = $name . str_repeat(' ', 25 - strlen($name));
            } else {
                $paddedName = $name;
            }

            foreach ($values as $value) {
                $this->showResponseHeader(
                    $paddedName,
                    $value,
                    in_array($name, ['Host', 'Connection', 'X-Powered-By', 'Server', 'Date', 'Cache-Control']) ? -1 : 0
                );
            }
        }

        return $response;
    }

    /**
     * @param $method
     * @param $uri
     * @param array $headers
     * @param $data
     * @return Response
     * @throws \Exception
     */
    public function request($method, $uri, array $headers = [], $data = '')
    {
        // Options
        $options = [
            'exceptions' => false,
            'headers' => $headers,
            'body' => $data
        ];

        if ($this->proxyServer !== null) {
            $options['proxy'] = $this->proxyServer;
        }

        // Generating request
        $request = $this->client->createRequest($method, $uri, $options);

        // Sending
        return $this->sendGuzzleRequest($request);
    }

    /**
     * Utility method to print status code
     *
     * @param int $code
     * @param string $message
     * @param int $level
     */
    public function showStatusCode($code, $message, $level = 1)
    {
        if ($this->writer !== null) {
            $this->writer->writep(new HttpStatusCode($code, $message), $level);
        }
    }

    /**
     * Utility method to show information with tag on the left
     *
     * @param string $tag
     * @param string $value
     * @param int $level
     */
    public function showTagged($tag, $value, $level = 1)
    {
        if ($this->writer !== null) {
            $this->writer->writep(new BeforeSendParameter($tag, $value), $level);
        }
    }

    /**
     * Utility method to show response header
     *
     * @param string $name
     * @param string $value
     * @param int $level
     */
    public function showResponseHeader($name, $value, $level = 1)
    {
        if ($this->writer !== null) {
            $this->writer->writep(new ResponseHeader($name, $value), $level);
        }
    }

    /**
     * Performs assertion against condition on Response
     *
     * @param boolean $condition
     * @param string $true
     * @param string $false
     * @param int $level
     * @return mixed
     */
    public function makeResponseAssertion($condition, $true, $false, $level = 1)
    {
        if ($this->writer !== null) {
            $this->writer->writep(new ResponseAssertion($condition, $true, $false), $level);
        }
        return $condition;
    }
}
