<?php

namespace Armor\Http\Chalk;

use Chalk\AnsiColor;
use Chalk\Pieces\AnsiColoredString;
use Chalk\Pieces\CompositePiece;
use Chalk\Pieces\StringPiece;

class ResponseHeader extends CompositePiece
{
    public function __construct($headerName, $value)
    {
        $pieces = [];
        $pieces[] = new StringPiece(' ');
        $pieces[] = new AnsiColoredString(' HDR ', AnsiColor::BLACK, AnsiColor::BLACK, AnsiColor::BOLD);
        $pieces[] = new StringPiece(' ');
        $pieces[] = new AnsiColoredString($headerName, AnsiColor::YELLOW);
        $pieces[] = new StringPiece(' ');
        $pieces[] = new AnsiColoredString($value, AnsiColor::GREEN);
        $pieces[] = new StringPiece("\n");

        parent::__construct($pieces);
    }
}
