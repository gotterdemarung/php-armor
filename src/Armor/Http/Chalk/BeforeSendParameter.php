<?php

namespace Armor\Http\Chalk;

use Chalk\AnsiColor;

class BeforeSendParameter extends ClientMessagePiece
{
    public function __construct($tag, $message)
    {
        parent::__construct(
            strtoupper($tag),
            AnsiColor::BLACK,
            AnsiColor::BLACK,
            AnsiColor::BOLD,
            $message
        );
    }
}
