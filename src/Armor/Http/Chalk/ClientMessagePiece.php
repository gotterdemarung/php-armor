<?php

namespace Armor\Http\Chalk;

use Chalk\Pieces\AnsiColoredString;
use Chalk\Pieces\CompositePiece;
use Chalk\Pieces\StringPiece;

abstract class ClientMessagePiece extends CompositePiece
{
    public function __construct($tag, $tagFg, $tagBg, $tagSpecial, $message)
    {
        $pieces = [];
        $pieces[] = new StringPiece(' ');
        $pieces[] = new AnsiColoredString(' ' . $tag . ' ', $tagFg, $tagBg, $tagSpecial);
        $pieces[] = new StringPiece(' ' . $message . "\n");

        parent::__construct($pieces);
    }
}
