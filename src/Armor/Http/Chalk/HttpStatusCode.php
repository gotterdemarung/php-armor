<?php

namespace Armor\Http\Chalk;

use Chalk\AnsiColor;

class HttpStatusCode extends ClientMessagePiece
{
    /**
     * @param int|string $code
     * @param string $message
     */
    public function __construct($code, $message)
    {
        if (!is_int($code)) {
            parent::__construct($code, AnsiColor::WHITE, AnsiColor::RED, AnsiColor::BOLD, $message);
        } elseif (intval($code / 100) !== 2) {
            parent::__construct($code, AnsiColor::BLACK, AnsiColor::YELLOW, AnsiColor::ITALIC, $message);
        } else {
            parent::__construct((string) $code, AnsiColor::WHITE, AnsiColor::GREEN, AnsiColor::BOLD, $message);
        }
    }
}
