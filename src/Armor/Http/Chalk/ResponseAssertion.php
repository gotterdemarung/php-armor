<?php

namespace Armor\Http\Chalk;

use Chalk\AnsiColor;
use Chalk\Pieces\AnsiColoredString;
use Chalk\Pieces\CompositePiece;
use Chalk\Pieces\StringPiece;

class ResponseAssertion extends CompositePiece
{
    public function __construct($condition, $true, $false)
    {
        $pieces = [];
        $pieces[] = new StringPiece(' ');
        if ($condition) {
            $pieces[] = new AnsiColoredString('  ✓  ', AnsiColor::GREEN, AnsiColor::BLACK);
            $pieces[] = new StringPiece(' ' . $true);
        } else {
            $pieces[] = new AnsiColoredString('  ❌  ', AnsiColor::RED, AnsiColor::BLACK);
            $pieces[] = new StringPiece(' ');
            $pieces[] = new AnsiColoredString($false, AnsiColor::RED);
        }
        $pieces[] = new StringPiece("\n");

        parent::__construct($pieces);
    }
}
