<?php

namespace Armor;

use Armor\Command\ArmorScript;
use Armor\Command\Covery;
use Armor\Command\Hash;
use Armor\Command\Psr2Check;
use Armor\Command\SelfUpdate;
use Armor\Command\SymfonyDiInspect;
use Armor\Command\TelegramBot;
use Armor\Command\TrunkStatus;
use Armor\Command\TrunkSwitch;
use K0re\LocalConfig\JsonLocalConfig;
use Stecman\Component\Symfony\Console\BashCompletion\CompletionCommand;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Input\ArgvInput;

class Armor
{
    public function up()
    {
        // Loading config
        $config = new JsonLocalConfig(".armorrc", false, true);

        $application = new Application('Armor', '0.2.1');
        $application->add(new CompletionCommand());
        $application->add(new SelfUpdate());
        $application->add(new Covery());
        $application->add(new ArmorScript());
        $application->add(new SymfonyDiInspect());
        $application->add(new Psr2Check());
        $application->add(new TrunkStatus());
        $application->add(new TrunkSwitch());
        $application->add(new TelegramBot());
        $application->add(new Hash());
        return $application->run(
            new InputConfigAdapter($config, new ArgvInput())
        );
    }
}
