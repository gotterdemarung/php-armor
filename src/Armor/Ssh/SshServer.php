<?php

namespace Armor\Ssh;

use Armor\Ssh\Chalk\SshCommunicationLog;
use Chalk\StreamWriter;
use phpseclib\Net\SSH2;
use phpseclib\System\SSH\Agent;

class SshServer
{
    private $name;
    private $username;
    private $hostname;
    private $folder;
    private $composerLib;

    private $connected = false;

    /**
     * @var SSH2
     */
    private $ssh;
    /**
     * @var Agent
     */
    private $agent;

    /**
     * @var StreamWriter|null
     */
    private $writer;

    /**
     * Constructor
     *
     * @param string $name
     * @param string $username
     * @param string $hostname
     * @param string $folder
     * @param string $composerLib
     * @param StreamWriter $writer
     * @throws \Exception
     */
    public function __construct($name, $username, $hostname, $folder, $composerLib, StreamWriter $writer = null)
    {
        if (!function_exists('ssh2_exec')) {
            throw new \Exception('SSH module not installed, run sudo apt-get install libssh2-php');
        }

        $this->name = $name;
        $this->username = $username;
        $this->hostname = $hostname;
        $this->folder = $folder;
        $this->composerLib = $composerLib;

        $this->writer = $writer;

        $this->ssh = new SSH2($hostname);
        $this->agent = new Agent();
        $this->agent->startSSHForwarding($this->ssh);
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getHostname()
    {
        return $this->hostname;
    }

    /**
     * Establishes SSH connection
     */
    public function connect()
    {
        if (!$this->connected) {
            $this->ssh->login($this->username, $this->agent);
            $this->connected = true;
        }
    }

    /**
     * Executes command
     *
     * @param string $cmd
     * @return string
     */
    private function exec($cmd)
    {

        if ($this->writer !== null) {
            $this->writer->writep(new SshCommunicationLog($cmd, true), 0);
        }

        $result = explode("\n", $this->ssh->exec($cmd));
        if ($this->writer !== null) {
            foreach ($result as $row) {
                $this->writer->writep(new SshCommunicationLog($row), 0);
            }
        }

        return implode("\n", $result);
    }

    /**
     * Returns current git status on server
     *
     * @return array
     */
    public function getStats()
    {
        $this->connect();
        $statArray = [];

        $lines = explode("\n", $this->exec('cd htdocs && git rev-parse HEAD && git rev-parse --abbrev-ref HEAD'));

        $statArray['commit'] = $lines[0];
        $statArray['branch'] = $lines[1];

        $lines = explode(':', trim($this->exec('cd htdocs && grep -e "' . $this->composerLib . '" composer.json')));
        $statArray['libBranch'] = trim(str_replace(['"', ','], '', $lines[1]));

        $lines = explode("\n", $this->exec('cd htdocs && grep -e "' . $this->composerLib . '" -A 5 composer.lock'));
        $lines = explode(':', trim($lines[5]));
        $statArray['libCommit'] = trim(str_replace(['"', ','], '', $lines[1]));

        return $statArray;
    }

    /**
     * Switches git branch
     *
     * @param string $branch
     * @param string $libBranch
     * @return array
     */
    public function switchBranch($branch, $libBranch = null)
    {
        $this->connect();
        $localName = $branch;
        if (substr($branch, 0, 7) === "origin/") {
            $localName = substr($branch, 7);
        } else {
            $localName = $branch;
            $branch = "origin/" . $branch;
        }

        $lines = explode("\n", $this->exec("cd htdocs && git fetch && git checkout -b --force $localName $branch"));
        $lines = array_merge(
            $lines,
            explode("\n", $this->exec("cd htdocs && git checkout --force $localName && git pull"))
        );
        if ($libBranch !== null) {
            $lib = str_replace("/", "\\/", $this->composerLib);
            $lines[] = $this->exec(
                "cd htdocs && sed -i -- 's/\"{$lib}\\\":\\s*\"dev-[^\"]*\"/\"{$lib}\\\": \"dev-{$libBranch}\"/g'"
                . ' composer.json'
            );

            $lines[] = $this->exec('cd htdocs && composer update --no-ansi --no-progress --no-interaction');
        }

        return $lines;
    }
}
