<?php

namespace Armor\Ssh\Chalk;

use Armor\Ssh\SshServer;
use Chalk\AnsiColor;
use Chalk\Pieces\AnsiColoredString;
use Chalk\Pieces\CompositePiece;

class TrunkStatusPiece extends CompositePiece
{
    public function __construct(SshServer $origin, array $data)
    {
        $list = [];
        $list[] = new AnsiColoredString('Server status for ', AnsiColor::WHITE);
        $list[] = new AnsiColoredString($origin->getName() . "\n", AnsiColor::WHITE, null, AnsiColor::BOLD);

        if ($data['branch'] === 'master' || $data['branch'] === 'live') {
            $list[] = new AnsiColoredString('  ✓  ', AnsiColor::GREEN, null, AnsiColor::BOLD);
        } else {
            $list[] = new AnsiColoredString('  ❌  ', AnsiColor::YELLOW, null, AnsiColor::BOLD);
        }
        $list[] = new AnsiColoredString('Source points to ', AnsiColor::WHITE);
        $list[] = new AnsiColoredString($data['branch'] . ' ', AnsiColor::CYAN, null, AnsiColor::BOLD);
        $list[] = new AnsiColoredString(substr($data['commit'], 0, 7) . "\n", AnsiColor::YELLOW, null, AnsiColor::BOLD);
        if ($data['libBranch'] === 'dev-master' || $data['libBranch'] === 'dev-live') {
            $list[] = new AnsiColoredString('  ✓  ', AnsiColor::GREEN, null, AnsiColor::BOLD);
        } else {
            $list[] = new AnsiColoredString('  ❌  ', AnsiColor::YELLOW, null, AnsiColor::BOLD);
        }
        $list[] = new AnsiColoredString('Library points to ', AnsiColor::WHITE);
        $list[] = new AnsiColoredString($data['libBranch'] . ' ', AnsiColor::CYAN, null, AnsiColor::BOLD);
        $list[] = new AnsiColoredString(
            substr($data['libCommit'], 0, 7) . "\n",
            AnsiColor::YELLOW,
            null,
            AnsiColor::BOLD
        );

        parent::__construct($list);
    }
}
