<?php

namespace Armor\Ssh\Chalk;

use Chalk\AnsiColor;
use Chalk\Pieces\AnsiColoredString;
use Chalk\Pieces\CompositePiece;

class SshCommunicationLog extends CompositePiece
{
    public function __construct($messages, $out = false)
    {
        if (!is_array($messages)) {
            $messages = [$messages];
        }

        $list = [];
        foreach ($messages as $m) {

            if ($out) {
                $list[] = new AnsiColoredString(' ===> ', AnsiColor::BLUE, null, AnsiColor::BOLD);
            } else {
                $list[] = new AnsiColoredString(' <=== ', AnsiColor::GREEN, null, AnsiColor::BOLD);
            }

            $list[] = new AnsiColoredString($m . "\n", $out ? AnsiColor::BLUE : AnsiColor::GREEN);
        }

        parent::__construct($list);
    }
}
