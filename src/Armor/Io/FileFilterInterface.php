<?php

namespace Armor\Io;

interface FileFilterInterface
{
    /**
     * @param \SplFileInfo $fileInfo
     * @return boolean
     */
    public function filter(\SplFileInfo $fileInfo);
}
