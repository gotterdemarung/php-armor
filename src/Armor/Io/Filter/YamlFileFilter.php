<?php

namespace Armor\Io\Filter;

use Armor\Io\FileFilterInterface;

class YamlFileFilter implements FileFilterInterface
{
    /**
     * @param \SplFileInfo $fileInfo
     * @return boolean
     */
    public function filter(\SplFileInfo $fileInfo)
    {
        return strtolower($fileInfo->getExtension()) === 'yml';
    }
}
