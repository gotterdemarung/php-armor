<?php

namespace Armor\Util\Phpcs;

/**
 * Class CliAdapter
 *
 * Utility class, used to invoke code sniffer inspections programically
 *
 * @package Armor\Util\Phpcs
 */
class CliAdapter extends \PHP_CodeSniffer_CLI
{
    public function __construct(array $files)
    {
        $this->values =  array_merge(
            $this->getDefaults(),
            [
                'standard' => ['PSR2'],
                'showProgress' => true,
                'verbosity' => 0,
                'files' => $files
            ]
        );
    }
}
