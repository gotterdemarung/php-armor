<?php

namespace Armor\Log;

use Psr\Log\AbstractLogger;

class CallbackLogger extends AbstractLogger
{
    /**
     * @var callable
     */
    private $callback;

    /**
     * @param callable $callback
     */
    public function __construct($callback)
    {
        if (!is_callable($callback)) {
            throw new \InvalidArgumentException('Callable expected');
        }
        $this->callback = $callback;
    }


    /**
     * Logs with an arbitrary level.
     *
     * @param mixed $level
     * @param string $message
     * @param array $context
     * @return null
     */
    public function log($level, $message, array $context = array())
    {
        call_user_func_array($this->callback, [$level, $message, $context]);
    }
}
