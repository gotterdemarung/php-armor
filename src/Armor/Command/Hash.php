<?php

namespace Armor\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Hash extends Command
{
    protected function configure()
    {
        $this->setName('hash')
            ->addArgument('target', InputArgument::REQUIRED, 'Target string')
            ->setDescription('Performs hashing');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $v = $input->getArgument('target');
        $output->writeln('');
        $output->writeln(
            sprintf(
                " <fg=white;options=bold>%s</fg=white;options=bold> \"<fg=yellow>%s</fg=yellow>\"",
                "Hashing string",
                $v
            )
        );
        $output->writeln('');

        $output->writeln(
            sprintf(
                " <fg=red;options=bold>%s</fg=red;options=bold> <fg=cyan;options=bold>%s</fg=cyan;options=bold>",
                "crc32           ",
                crc32($v)
            )
        );

        if (!function_exists('hash')) {
            $output->writeln('');
            return 0;
        }

        foreach (hash_algos() as $alg) {
            $r = hash($alg, $v, false);
            $output->writeln(
                sprintf(
                    " <fg=red;options=bold>%-12s</fg=red;options=bold> "
                    . "<fg=blue;options=bold>%3d</fg=blue;options=bold> "
                    . "<fg=green;options=bold>%s</fg=green;options=bold>"
                    . "<fg=cyan;options=bold>%s</fg=cyan;options=bold>",
                    $alg,
                    strlen($r),
                    $r,
                    strlen($r) <= 8 ? ' [int: ' . hexdec($r) . ']' : ''
                )
            );
        }

        $output->writeln('');
        return 0;
    }
}
