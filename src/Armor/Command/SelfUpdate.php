<?php

namespace Armor\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SelfUpdate extends Command
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('self-update')->setDescription('Performs self update');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $folder = realpath(__DIR__ . '/../../../');
        chdir($folder);
        shell_exec('git fetch && git merge origin/master && composer install');
    }
}
