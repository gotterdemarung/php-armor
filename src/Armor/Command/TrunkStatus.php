<?php

namespace Armor\Command;

use Armor\Ssh\Chalk\TrunkStatusPiece;
use Armor\Ssh\SshServer;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class TrunkStatus extends AbstractCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('trunk')
            ->setDescription('Display trunk statuses')
            ->addArgument('name', InputArgument::OPTIONAL, 'Trunk names, comma separated');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $writer = $this->getWriter($output);
        $logger = $this->getLogger($writer);

        $config = $input->getOption('config:trunks');
        $all = array_keys($config);
        $logger->debug('Configured :count trunks', ['count' => count($config)]);

        $name = $input->getArgument('name');
        $folder = 'htdocs';
        $username = $input->getOption('config:trunks.username');
        $lib = $input->getOption('config:trunks.lib');

        $logger->debug('Lookup folder is :folder, library is :lib', ['folder' => $folder, 'lib' => $lib]);
        if (empty($name)) {
            $name = $all;
        } else {
            $name = explode(',', $name);
        }

        // Validating
        foreach ($name as $x) {
            if (!isset($config[$x])) {
                throw new \InvalidArgumentException("Trunk {$x} not configured");
            }
        }

        // Querying
        foreach ($name as $x) {
            $logger->debug('Entering trunk :name', ['name' => $x]);

            $hostname = $config[$x];
            $ssh = new SshServer($x, $username, $hostname, $folder, $lib, $writer);
            $logger->debug(
                'Authenticating :user @ :hostname for :name',
                ['user' => $username, 'name' => $x, 'hostname' => $hostname]
            );

            $stat = $ssh->getStats();
            $writer->writep(new TrunkStatusPiece($ssh, $stat));
            $writer->writeln();
        }
    }
}
