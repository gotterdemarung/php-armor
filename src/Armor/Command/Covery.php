<?php

namespace Armor\Command;

use Armor\Http\HttpClient;
use Chalk\Pieces\DumpPiece;
use Chalk\StreamWriter;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class Covery extends AbstractCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('covery')
            ->setDescription('Performs API request to Covery')
            ->addOption(
                'assertHttpCode',
                null,
                InputOption::VALUE_OPTIONAL,
                'If set, performs assertion against HTTP status code',
                null
            )
            ->addOption(
                'proxy',
                null,
                InputOption::VALUE_OPTIONAL,
                'Proxy server address',
                null
            )
            ->addArgument('server', InputArgument::REQUIRED, 'Server name')
            ->addArgument('method', InputArgument::REQUIRED, 'Request method [GET|POST|PUT etc]')
            ->addArgument('url', InputArgument::REQUIRED, 'Endpoint url')
            ->addArgument('body', InputArgument::OPTIONAL, 'Payload', '');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // Building objects
        $writer = StreamWriter::buildFromSymfonyOutput($output);
        $client = new HttpClient($input->getOption('proxy') ? $input->getOption('proxy') : null, $writer);
        $output->writeln('');

        // Reading configs
        $server = $input->getOption('config:covery.servers')[$input->getArgument('server')]['server'];
        $token  = $input->getOption('config:covery.servers')[$input->getArgument('server')]['token'];
        $secret = $input->getOption('config:covery.servers')[$input->getArgument('server')]['secret'];
        $inodes = $input->getOption('config:covery.servers')[$input->getArgument('server')]['inodes'];

        $assertCode = intval($input->getOption('assertHttpCode'));
        $url = $input->getArgument('url');
        if (substr($url, 0, 1) === '/') {
            $url = substr($url, 1);
        }
        $method = strtoupper($input->getArgument('method'));
        $url = $server . (substr($server, -1) !== '/' ? '/' : '') . $url;

        $data = $input->getArgument('body');
        $nonce = microtime(true);
        $sig = hash('sha256', $nonce . $data . $secret);

        $headers = [
            'X-Auth-Token' => $token,
            'X-Auth-Nonce' => $nonce,
            'X-Auth-Signature' => $sig,
            'X-Identities' => $inodes
        ];

        // Show pre-headers
        $client->showTagged('tkn', $token);
        $client->showTagged('ids', $inodes, 0);
        $client->showTagged('rnd', $nonce, 0);
        $client->showTagged('sig', $sig, 0);

        // Sending request
        $start = microtime(true);
        try {
            $response = $client->request(
                $method,
                $url,
                $headers,
                $data
            );
        } catch (\Exception $error) {
            return 11;

        }
        $delta = microtime(true) - $start;

        $body = $response->getBody() !== null ? $response->getBody()->getContents() : '';
        // Attempting to decode JSON
        $json = null;
        if (!empty($body)) {
            $json = json_decode($body, true);
            if (is_array($json)) {
                $msg = 'Valid JSON';
                $body = json_encode($json, JSON_PRETTY_PRINT);
            } else {
                $msg = 'Plain ' . strlen($body) . ' bytes';
            }
        } else {
            $msg = 'Empty content';
        }

        $delta = sprintf("%.3f sec", $delta);
        $client->showTagged('inf', "{$msg} received in {$delta}", 0);

        // Assertions
        $assertFailed = false;
        if ($assertCode > 0) {
            $assertFailed = $assertFailed || !$client->makeResponseAssertion(
                $response->getStatusCode() === $assertCode,
                "Status code {$assertCode}, as expected",
                "Expected {$assertCode} status code, but received " . $response->getStatusCode()
            );
        }

        // Body
        $writer->writeln();
        if ($json !== null) {
            $writer->writep(new DumpPiece($json));
            $writer->writeln();
        } elseif ($body !== '') {
            $writer->writeln($body);
        }

        return $assertFailed ? 22 : 0;
    }
}
