<?php

namespace Armor\Command;

use Chalk\StreamWriter;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ArmorScript extends AbstractCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('script')
            ->setDescription('Runs armorscript')
            ->addArgument('file', InputArgument::REQUIRED, 'Target script file');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $filename = $input->getArgument('file');
        $file = new \SplFileInfo($filename);
        if (!$file->isFile()) {
            throw new \Exception("File {$filename} not found");
        }
        $file = new \SplFileInfo($file->getRealPath());

        // Building writer
        $writer = StreamWriter::buildFromSymfonyOutput($output);

        $autoLoad = $this->findForeignAutoLoader($file->getPathInfo());
        if ($autoLoad !== null) {
            /** @noinspection PhpIncludeInspection */
            include($autoLoad->getRealPath());
        }

        /** @noinspection PhpIncludeInspection */
        $func = include($file->getRealPath());
        if (!is_callable($func)) {
            throw new \Exception("File {$filename} must return callable to be Armor-compatible");
        }

        // Building utility objects
        $util = $this->getServiceLocator($input, $writer);

        // Resolving arguments
        $args = [];
        foreach ($this->getReflection($func)->getParameters() as $param) {
            if (isset($util[$param->getName()])) {
                if (is_callable($util[$param->getName()])) {
                    $util[$param->getName()] = $util[$param->getName()]();
                }
                $args[] = $util[$param->getName()];
            } else {
                throw new \Exception('Unable to map parameter $' . $param->getName());
            }
        }

        call_user_func_array($func, $args);
    }
}
