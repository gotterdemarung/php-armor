<?php

namespace Armor\Command;

use Armor\Http\HttpClient;
use Chalk\StreamWriter;
use GuzzleHttp\Message\Request;
use GuzzleHttp\Stream\Stream;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class TelegramBot extends AbstractCommand
{
    const BASE_URL = 'https://api.telegram.org/bot';

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('tsend')
            ->setDescription('Telegram bot')
            ->addArgument('target', InputArgument::REQUIRED, 'Id of user or chat group')
            ->addArgument('text', InputArgument::REQUIRED, 'Message text')
            ->addOption('isGroup', 'g', InputOption::VALUE_NONE, 'Is group chat')
            ;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $token = $input->getOption('config:telegram.token');
        $target = $input->getArgument('target');
        if ($input->getOption('isGroup')) {
            $target = -$target;
        }
        $text = $input->getArgument('text');
        $writer = StreamWriter::buildFromSymfonyOutput($output)->withDeltaThreshold(1);
        $client = new HttpClient(null, $writer);

        // Making request
        $req = new Request(
            "POST",
            self::BASE_URL . $token . '/getMe'
        );

        // Checking auth
        $res = $client->sendGuzzleRequest($req);
        if ($res->getStatusCode() !== 200) {
            $client->showTagged('err', 'Not 200 received', 2);
            return 1;
        }
        $data = json_decode($res->getBody(), true);
        if (!$data['ok']) {
            $client->showTagged('err', $data['Description'], 2);
            return 2;
        } else {
            $client->showTagged('bot', 'My name is ' . $data['result']['first_name'], 2);
        }

        $req = new Request(
            "POST",
            self::BASE_URL . $token . '/sendMessage',
            ['Content-Type' => 'application/x-www-form-urlencoded; charset=UTF-8'],
            Stream::factory(http_build_query([
                'chat_id' => $target,
                'text'    => $text
            ]))
        );
        $res = $client->sendGuzzleRequest($req);
        if ($res->getStatusCode() === 400) {
            $data = json_decode($res->getBody(), true);
            $client->showTagged('err', $data['description'], 2);
            return 10;
        } elseif ($res->getStatusCode() !== 200) {
            $client->showTagged('err', 'Not 200 received', 2);
            return 10;
        }

        return 0;
    }
}
