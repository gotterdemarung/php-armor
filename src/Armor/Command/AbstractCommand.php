<?php

namespace Armor\Command;

use Armor\Http\HttpClient;
use Armor\Log\CallbackLogger;
use Chalk\Pieces\LogEntry;
use Chalk\StreamWriter;
use Psr\Log\LogLevel;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

abstract class AbstractCommand extends Command
{
    /**
     * Creates writer for provided output
     *
     * @param OutputInterface $out
     * @return StreamWriter
     */
    public function getWriter(OutputInterface $out)
    {
        return StreamWriter::buildFromSymfonyOutput($out);
    }

    /**
     * Returns logger, built on top of provided writer
     *
     * @param StreamWriter $writer
     * @return CallbackLogger
     */
    public function getLogger(StreamWriter $writer)
    {
        return new CallbackLogger(
            function ($level, $message, array $context) use (&$writer) {
                $writer->writep(new LogEntry($level, $message, $context), $level === LogLevel::DEBUG ? 0 : 1);
            }
        );
    }

    /**
     * Returns service locator array
     * Common usage - armorscript
     *
     * @param InputInterface $input
     * @param StreamWriter $writer
     * @return array
     */
    public function getServiceLocator(InputInterface $input, StreamWriter $writer)
    {
        $util = [];
        $util['out'] = $writer;
        $util['logger'] = function () use (&$writer) {
            return $this->getLogger($writer);
        };

        $util['pdo'] = function () use ($input) {
            // Building PDO object
            $pdo = new \PDO(
                $input->getOption('config:pdo.dsn'),
                $input->getOption('config:pdo.user'),
                $input->getOption('config:pdo.password')
            );
            $pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            return $pdo;
        };

        $util['httpClient'] = function () use ($writer) {
            return new HttpClient(null, $writer);
        };

        return $util;
    }

    /**
     * @param object|callable $callable
     * @return \ReflectionFunction|\ReflectionMethod
     */
    public function getReflection($callable)
    {
        if (is_array($callable)) {
            return new \ReflectionMethod($callable[0], $callable[1]);
        } elseif (is_string($callable)) {
            return new \ReflectionFunction($callable);
        } elseif (is_a($callable, 'Closure') || is_callable($callable, '__invoke')) {
            $objReflector = new \ReflectionObject($callable);
            return $objReflector->getMethod('__invoke');
        }

        throw new \InvalidArgumentException("Not a callable");
    }

    /**
     * Automatically finds composer autoloader
     *
     * @param \SplFileInfo $path
     * @return null|\SplFileInfo
     */
    public function findForeignAutoLoader(\SplFileInfo $path)
    {
        if (!is_dir($path)) {
            return null;
        }

        $file = new \SplFileInfo($path->getRealPath() . '/vendor/autoload.php');

        if ($file->isFile()) {
            return $file;
        }

        return null;
    }
}
