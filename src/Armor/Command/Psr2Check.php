<?php

namespace Armor\Command;

use Armor\Util\Phpcs\CliAdapter;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Psr2Check extends AbstractCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('psr2')
            ->addArgument('target', InputArgument::OPTIONAL, 'Target folder/file to scan', null)
            ->setDescription('Performs assertion against PSR2 rules');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // Util
        $writer = $this->getWriter($output);

        $base = new \SplFileInfo($input->getArgument('target') === null ? './' : $input->getArgument('target'));
        if ($base->isFile()) {
            $writer->writeln('Received target file ' . $base->getRealPath(), 0);
            $target = [$base->getRealPath()];
        } elseif ($base->isDir()) {
            $target = [];
            // Searching for composer.json
            $composerFile = new \SplFileInfo($base->getRealPath() . '/composer.json');
            if ($composerFile->isFile()) {
                $json = json_decode(file_get_contents($composerFile->getRealPath()), true);
                $target = [];
                if (isset($json['autoload'])) {
                    if (isset($json['autoload']['psr-0'])) {
                        foreach ($json['autoload']['psr-0'] as $path) {
                            $writer->writeln('Found PSR0 path ' . $path, 0);
                            $target[] = $base->getRealPath() . '/' . $path;
                        }
                    }
                    if (isset($json['autoload']['psr-4'])) {
                        foreach ($json['autoload']['psr-4'] as $path) {
                            $writer->writeln('Found PSR4 path ' . $path, 0);
                            $target[] = $base->getRealPath() . '/' . $path;
                        }
                    }
                }
            }

            $target = array_unique($target);

            if (empty($target)) {
                $target[] = $base->getRealPath();
            }

        } else {
            throw new \InvalidArgumentException("Unable to find targer " . $input->getArgument('target'));
        }

        foreach ($target as $path) {
            $writer->writeln('Using path ' . $path, 0);
        }

        // Increase nesting level for XDebug
        ini_set('xdebug.max_nesting_level', 1000);

        // Building CLI to be compatible with fucking phpcs design
        $cli = new CliAdapter($target);
        return $cli->process() > 0 ? 1 : 0;
    }
}
