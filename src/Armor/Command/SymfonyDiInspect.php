<?php

namespace Armor\Command;

use Armor\Io\Filter\YamlFileFilter;
use Chalk\Pieces\HashMapPiece;
use Chalk\Pieces\SectionHeader;
use Chalk\StreamWriter;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\DependencyInjection\Reference;

class SymfonyDiInspect extends AbstractCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('di')
            ->setDescription('Dependency injection analysis')
            ->addOption('folder', null, InputOption::VALUE_OPTIONAL, 'Target path with configuration files', '.')
            ->addOption('params', null, InputOption::VALUE_OPTIONAL, 'Show parameters with its values', true)
            ->addOption('emptyCheck', null, InputOption::VALUE_OPTIONAL, 'Traits empty parameter values as error', true)
            ->addArgument('mainClass', InputArgument::OPTIONAL, 'Main class name');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // Building writer
        $writer = StreamWriter::buildFromSymfonyOutput($output);

        // Util
        $output->writeln('');

        // Params
        $folder = $input->getOption('folder');
        $mainClass = $input->getArgument('mainClass');
        $showParamsFlag = $input->getOption('params') === true || $input->getOption('params') === 'true';
        $emptyCheckFlag = $input->getOption('emptyCheck') === true || $input->getOption('emptyCheck') === 'true';

        // Reading all configuration files
        $path = realpath($folder);
        $iterator = new \RecursiveIteratorIterator(
            new \RecursiveDirectoryIterator($path),
            \RecursiveIteratorIterator::SELF_FIRST
        );

        $configs = [];
        foreach ($iterator as $fileInfo) {
            /** @var \SplFileInfo $fileInfo */
            if ($fileInfo->isDir()) {
                continue;
            }

            $configs[] = $fileInfo;
        }

        // Filtering
        $filter = new YamlFileFilter();
        $configs = array_filter($configs, [$filter, 'filter']);

        // Sorting
        uasort($configs, function (\SplFileInfo $a, \SplFileInfo $b) {
            $cA = substr_count(str_replace(['\\', '/'], '.', $a->getRealPath()), '.');
            $cB = substr_count(str_replace(['\\', '/'], '.', $b->getRealPath()), '.');

            if ($cA > $cB) {
                return 1;
            } elseif ($cA < $cB) {
                return -1;
            } else {
                return 0;
            }
        });

        // Reading YAML
        $container = new ContainerBuilder();
        $loader = new YamlFileLoader($container, new FileLocator());
        $writer->writep(new SectionHeader('Configuration files list'));
        foreach ($configs as $fileInfo) {
            $writer->writeln(' Reading ' . $fileInfo->getRealPath());
            $loader->load($fileInfo->getRealPath());
        }
        $writer->writeln();

        if ($showParamsFlag) {
            $writer->writep(new SectionHeader('Parameter values'));
            $writer->writep(new HashMapPiece($container->getParameterBag()->all(), true));
            $writer->writeln();
        }

        // Determining dependency problems
        $problems = [];
        $writer->writep(new SectionHeader('Container trace'), 0);
        foreach ($container->getDefinitions() as $name => $def) {
            $className = $def->getClass();
            $writer->writeln(" * Service [{$name}] backed by {$className}", 0);

            // Reading dependencies
            $deps = $this->findDependencies($def);
            if (count($deps) > 0) {
                foreach ($deps as $d) {
                    $writer->writeln("   Depends on " . $d, 0);
                    if (!$container->hasDefinition($d->__toString())) {
                        $problems[] = "Service {$name} requires not configured {$d}";
                    }
                }
            }

            $params = $this->findParameters($def);
            if (count($params) > 0) {
                foreach ($params as $p) {
                    $writer->writeln("   Uses parameter " . $p, 0);
                    if (!$container->hasParameter($p)) {
                        $problems[] = "Service {$name} requires missing parameter %{$p}%";
                    } elseif ($emptyCheckFlag) {
                        $value = $container->getParameter($p);
                        if (empty($value)) {
                            $problems[] = "Service {$name} requires parameter %{$p}%, which is empty";
                        }
                    }
                }
            }
        }
        $writer->writeln('', 0);

        if (count($problems) > 0) {
            $writer->writep(new SectionHeader('Problem list'), 10);
            foreach ($problems as $problem) {
                $writer->writeln($problem, 10);
            }

            return 5;
        }

        return 0;
    }

    /**
     * @param Definition $def
     * @return Reference[] $refs
     */
    public function findDependencies(Definition $def)
    {
        $deps = [];
        if ($def->getFactory() !== null) {
            $deps[] = $def->getFactory()[0];
        }
        foreach ($def->getArguments() as $arg) {
            if ($arg instanceof Reference) {
                $deps[] = $arg;
            }
        }
        foreach ($def->getMethodCalls() as $call) {
            if (isset($call[1]) && is_array($call[1])) {
                foreach ($call[1] as $arg) {
                    if ($arg instanceof Reference) {
                        $deps[] = $arg;
                    }
                }
            }
        }

        return array_unique($deps);
    }

    /**
     * @param Definition $def
     * @return string[]
     */
    public function findParameters(Definition $def)
    {
        $params = [];
        foreach ($def->getArguments() as $arg) {
            if (is_string($arg) && strlen($arg) > 2 && $arg[0] === '%') {
                $params[] = substr($arg, 1, strlen($arg) - 2);
            }
        }

        return $params;
    }
}
